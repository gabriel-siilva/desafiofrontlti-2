import { Routes } from "@angular/router";

import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "./login/login.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { TableComponent } from "./table/table.component";
import { AuthGuard } from "./guards/auth-guard";

export const appRoutes : Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
    },

    {
        path: 'login',
        component: LoginComponent,
        
    },

    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },

    {
        path: '**',
        component: PageNotFoundComponent
    }
]