import { Injectable } from '@angular/core';
import { Job, Reminder } from '.';

@Injectable()
export class GeralService {

  jobs: Array<Job> = new Array<Job>();
  reminders: Array<Reminder> = new Array<Reminder>();

  constructor() { }

  addJob(job) {
    this.jobs.push(job);
  }

  deleteJob(id: number) {
    this.jobs.splice(id, 1);
    for (let i = id; i < this.jobs.length; i++) {
      this.jobs[i].id = this.jobs[i].id - 1;
    }
  }

  addReminder(reminder) {
    this.reminders.push(reminder);
  }

  deleteReminder(id: number) {
    this.reminders.splice(id, 1);
    for (let i = id; i < this.reminders.length; i++) {
      this.reminders[i].id = this.reminders[i].id - 1;
    }
  }
}
