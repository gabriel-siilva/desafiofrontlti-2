import { Component, OnInit } from '@angular/core';
import { GeralService } from '../geral.service';
import { Reminder } from '..';

@Component({
  selector: 'app-reminders',
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.css']
})
export class RemindersComponent implements OnInit {

  reminders : Array<Reminder> = new Array<Reminder>();

  constructor(private service : GeralService) {
    this.reminders = this.service.reminders;
  }

  ngOnInit() {
  }

  deleteReminder(id : number) {
    this.service.deleteReminder(id);
  }

}
