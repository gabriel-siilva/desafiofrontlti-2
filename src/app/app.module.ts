import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { RemindersComponent } from './reminders/reminders.component';
import { TableComponent } from './table/table.component';

import { appRoutes } from './routes';
import { GeralService } from "./geral.service";
import { AuthService } from './login/auth.service';
import { AuthGuard } from './guards/auth-guard';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    LoginComponent,
    PageNotFoundComponent,
    RemindersComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot( appRoutes )
  ],
  providers: [ 
    GeralService, 
    AuthService, 
    AuthGuard,
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
