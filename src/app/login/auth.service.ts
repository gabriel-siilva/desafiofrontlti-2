import { Injectable } from '@angular/core';
import { User } from '..';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  userAutenticado: boolean;

  constructor(private router: Router) { }

  autenticarUser(user: User) {
    if (user.login == "Gabriel" && user.password == "1234") {
      this.userAutenticado = true;
      this.router.navigate(["/dashboard"]);
    } else {
      this.userAutenticado = false;
      alert("Usuário não autenticado");
    }
  }

  userIsAuth() {
    return this.userAutenticado;
  }
}
