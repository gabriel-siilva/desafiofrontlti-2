import { Component, OnInit } from '@angular/core';
import { User } from '..';
import { AuthService } from './auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user : User;

  constructor(private authService : AuthService) { }

  ngOnInit() {
    this.user = new User();
  }

  loginUser(){
    this.authService.autenticarUser(this.user);
    this.ngOnInit();
  }
}
