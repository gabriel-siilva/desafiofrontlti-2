import { Component, OnInit, Input, Output, Injectable} from '@angular/core';
import { Job } from '..';
import { GeralService } from '../geral.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  jobs: Array<Job> = new Array<Job>();
  valorTotal: number = 0;

  constructor(private service: GeralService) {
    this.jobs = this.service.jobs;
  }

  ngOnInit() {
  }

  deleteJob(id: number) {
    this.service.deleteJob(id);
  }

  getValorTotal() {
    for (let i = 0; i < this.jobs.length; i++) {
      this.valorTotal += this.jobs[i].valor;
    }
  }
}
