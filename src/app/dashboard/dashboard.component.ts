import { Component, OnInit } from '@angular/core';
import { Job, Reminder } from '..';
import { GeralService } from "../geral.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  job: Job;
  reminder: Reminder;

  constructor(private service: GeralService) {
  }

  ngOnInit() {
    this.job = new Job();
    this.reminder = new Reminder();
  }

  recebeJob() {
    this.job.id = this.service.jobs.length;
    this.service.addJob(this.job);
    this.ngOnInit();
  }

  recebeReminder() {
    this.reminder.id = this.service.reminders.length;
    this.service.addReminder(this.reminder);
    this.ngOnInit();
  }

}
